import { Fragment, useState } from "react";
import EditRoastingForm from "./EditRoastingForm";
import showResults from "../../showResults/showResults";
import UserService from "../../../services/user.service";
import { useParams } from "react-router";

const EditRoasting = () => {
  const { id } = useParams();

  const [certificateFile, setCertificate] = useState("");
  const [filesGambar, imageFileGambar] = useState("");

  const onFileChangeGambar = (file) => {
    imageFileGambar(file);
  };

  const handleSubmit = async (values) => {
    const formData = new FormData();

    if (values.roastingName != null) {
      formData.append("roasting_name", values.roastingName);
    }
    if (values.roastingProfile != null) {
      formData.append("roasting_profile_id", values.roastingProfile);
    }
    if (values.flavorNote != null) {
      formData.append("flavor_note", values.flavorNote);
    }
    if (values.acidity != null) {
      formData.append("acidity", values.acidity);
    }
    if (values.intensity != null) {
      formData.append("intensity", values.intensity);
    }
    if (values.tgl_roasting != null) {
      formData.append("tgl_roasting", values.tgl_roasting);
    }
    if (values.bean != null) {
      formData.append("bean_id", values.bean);
    }
    if (certificateFile != undefined) {
      for (let i = 0; i < certificateFile.length; i++) {
        formData.append("certificate[]", certificateFile[i]);
      }
    }
    if (filesGambar != "") {
      formData.append("files_gambar", filesGambar);
      formData.append("fileName_gambar", filesGambar.name);
    }

    UserService.editRoastingProduct(id, formData);
    showResults("Data has been changed");
  };

  const onCertificateChange = (file) => {
    setCertificate(file);
  };

  return (
    <Fragment>
      <EditRoastingForm
        onSubmit={handleSubmit}
        roastingID={id}
        onSelectCertificate={onCertificateChange}
        onSelectImageGambar={onFileChangeGambar}
      />
    </Fragment>
  );
};

export default EditRoasting;
