export const AddProduct = [
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "name": "productArr",
    "outputs": [
      {
        "internalType": "address",
        "name": "kopiAddress",
        "type": "address"
      },
      {
        "internalType": "string",
        "name": "dataHash",
        "type": "string"
      },
      {
        "internalType": "string",
        "name": "created_at",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "dataHash",
        "type": "string"
      },
      {
        "internalType": "string",
        "name": "created_at",
        "type": "string"
      }
    ],
    "name": "tambahProduct",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "listProduct",
    "outputs": [
      {
        "components": [
          {
            "internalType": "address",
            "name": "kopiAddress",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "dataHash",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "created_at",
            "type": "string"
          }
        ],
        "internalType": "struct AddProduct.Product[]",
        "name": "",
        "type": "tuple[]"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "datahash",
        "type": "string"
      }
    ],
    "name": "detailproductByHash",
    "outputs": [
      {
        "components": [
          {
            "internalType": "address",
            "name": "kopiAddress",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "dataHash",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "created_at",
            "type": "string"
          }
        ],
        "internalType": "struct AddProduct.Product",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "lisProductByAddress",
    "outputs": [
      {
        "components": [
          {
            "internalType": "address",
            "name": "kopiAddress",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "dataHash",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "created_at",
            "type": "string"
          }
        ],
        "internalType": "struct AddProduct.Product[]",
        "name": "",
        "type": "tuple[]"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "created_at",
        "type": "string"
      }
    ],
    "name": "lisProductByDate",
    "outputs": [
      {
        "components": [
          {
            "internalType": "address",
            "name": "kopiAddress",
            "type": "address"
          },
          {
            "internalType": "string",
            "name": "dataHash",
            "type": "string"
          },
          {
            "internalType": "string",
            "name": "created_at",
            "type": "string"
          }
        ],
        "internalType": "struct AddProduct.Product[]",
        "name": "",
        "type": "tuple[]"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "string",
        "name": "dataHash",
        "type": "string"
      }
    ],
    "name": "verify",
    "outputs": [
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  }
]