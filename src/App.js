import React, { Component, Suspense, lazy } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";

import Login from "./components/login.component";

import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";

import { history } from "./helpers/history";

import "./scss/style.scss";
import { Fragment } from "react";

import "./components/Roaster/Roaster.css";
import "./components/Roaster/RoasterMedia.css";

//Header
const Header = lazy(() => import("./components/Header/Header"));

//Detail Roaster
const GetId = lazy(() => import("./components/Roaster/MainPage/GetID"));

const MasterDataLists = lazy(() =>
  import("./components/Roaster/MasterData/MasterDataLists")
);

const AddRoastingProfile = lazy(() =>
  import("./components/Roaster/MasterData/RoastingProfile/AddRoastingProfile")
);
const EditRoastingProfile = lazy(() =>
  import("./components/Roaster/MasterData/RoastingProfile/EditRoastingProfile")
);

const AddUnit = lazy(() =>
  import("./components/Roaster/MasterData/Unit/AddUnit")
);
const EditUnit = lazy(() =>
  import("./components/Roaster/MasterData/Unit/EditUnit")
);

const AddGroundWhole = lazy(() =>
  import("./components/Roaster/MasterData/WholeBeanGround/AddWholeBeanGround")
);
const EditWholeBeanGround = lazy(() =>
  import("./components/Roaster/MasterData/WholeBeanGround/EditWholeBeanGround")
);

const AddBeans = lazy(() =>
  import("./components/Roaster/MasterData/Beans/AddBeans")
);
const EditBeans = lazy(() =>
  import("./components/Roaster/MasterData/Beans/EditBeans")
);

const DaftarVarietas = lazy(() =>
  import("./components/Roaster/MasterData/Varietas/DaftarVarietas")
);
const EditVarietas = lazy(() =>
  import("./components/Roaster/MasterData/Varietas/EditVarietas")
);

const DaftarProses = lazy(() =>
  import("./components/Roaster/MasterData/Proses/DaftarProses")
);
const EditProses = lazy(() =>
  import("./components/Roaster/MasterData/Proses/EditProses")
);

const DaftarSupplier = lazy(() =>
  import("./components/Roaster/MasterData/Supplier/DaftarSupplier")
);
const EditSupplier = lazy(() =>
  import("./components/Roaster/MasterData/Supplier/EditSupplier")
);

const ProductLists = lazy(() =>
  import("./components/Roaster/Products/ProductLists")
);
const AddProducts = lazy(() =>
  import("./components/Roaster/Products/AddProducts")
);
const EditProduct = lazy(() =>
  import("./components/Roaster/Products/EditProducts")
);

const RoastingLists = lazy(() =>
  import("./components/Roaster/Roasting/RoastingLists")
);
const AddRoasting = lazy(() =>
  import("./components/Roaster/Roasting/AddRoasting")
);
const EditRoasting = lazy(() =>
  import("./components/Roaster/Roasting/EditRoasting")
);

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      currentUser: undefined,
    };

    history.listen((location) => {
      props.dispatch(clearMessage()); // clear message when changing location
    });
  }

  componentDidMount() {
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user,
      });
    }
  }

  logOut() {
    this.props.dispatch(logout());
  }

  render() {
    const { currentUser } = this.state;
    const { isLoggedIn } = this.props;

    if (!isLoggedIn) {
      // history.push("/login");
    } else {
      const user = JSON.parse(localStorage.getItem("user"));
      if (user !== null) {
        const exampleJWT = user.token;
        function getPayload(jwt) {
          return atob(jwt.split(".")[1]);
        }
        const payload = getPayload(exampleJWT);
        if (payload.exp < Date.now() / 1000) {
          localStorage.removeItem("user");
          history.push("/");
        }
      }
    }

    return (
      <Router history={history}>
        <Suspense fallback={<div>Loading...</div>}>
          <Route path="/" exact component={Login} />
          {currentUser && (
            <div>
              <Header logoutClick={this.logOut} />
            </div>
          )}
          <Route path="/detailRoasting/:code/:id" exact component={GetId} />

          {currentUser && (
            <Fragment>
              <Route path="/masterData" exact component={MasterDataLists} />

              <Route
                path="/masterData/addRoastingProfile"
                exact
                component={AddRoastingProfile}
              />
              <Route
                path="/masterData/editRoastingProfile/:id"
                exact
                component={EditRoastingProfile}
              />

              <Route
                path="/masterData/addWholeBean-Ground"
                exact
                component={AddGroundWhole}
              />
              <Route
                path="/masterData/editWholeBean-Ground/:id"
                exact
                component={EditWholeBeanGround}
              />

              <Route path="/masterData/addUnit" exact component={AddUnit} />
              <Route
                path="/masterData/editUnit/:id"
                exact
                component={EditUnit}
              />

              <Route path="/masterData/addBeans" exact component={AddBeans} />
              <Route
                path="/masterData/editBeans/:id"
                exact
                component={EditBeans}
              />

              <Route
                path="/masterData/addVariety"
                exact
                component={DaftarVarietas}
              />
              <Route
                path="/masterData/editVariety/:id"
                exact
                component={EditVarietas}
              />

              <Route
                path="/masterData/addProcess"
                exact
                component={DaftarProses}
              />
              <Route
                path="/masterData/editProcess/:id"
                exact
                component={EditProses}
              />

              <Route
                path="/masterData/addOrigin"
                exact
                component={DaftarSupplier}
              />
              <Route
                path="/masterData/editOrigin/:id"
                exact
                component={EditSupplier}
              />

              <Route path="/greenBeans" exact component={ProductLists} />
              <Route
                path="/greenBeans/addGreenBeans"
                exact
                component={AddProducts}
              />
              <Route
                path="/greenBeans/editGreenBeans/:id"
                exact
                component={EditProduct}
              />

              <Route path="/roasting" exact component={RoastingLists} />
              {currentUser && (
                <Route exact path="/roasting/addRoasting">
                  <AddRoasting code={currentUser.user_detail.group.code} />
                </Route>
              )}
              <Route
                path="/roasting/editRoasting/:id"
                exact
                component={EditRoasting}
              />
            </Fragment>
          )}
        </Suspense>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  const { isLoggedIn } = state.auth;
  return {
    user,
    isLoggedIn,
  };
}

export default connect(mapStateToProps)(App);
