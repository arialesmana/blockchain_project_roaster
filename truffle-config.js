require("dotenv").config();
const HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
    },
    ropsten: {
      provider: () => 
        new HDWalletProvider(
          process.env.REACT_APP_MNEMONIC, 
          `https://ropsten.infura.io/v3/${process.env.REACT_APP_INFURA_PROJECT_ID}`
        ),
      network_id: 3,
      gas: 5500000,
      confirmations: 2,
      timeoutBlocks: 200,
      skipDryRun: true
    },
  },
  mocha: {},
  compilers: {
    solc: {
      version: "^0.6.4",
    }
  }
};
