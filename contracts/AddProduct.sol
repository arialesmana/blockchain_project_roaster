// SPDX-License-Identifier: MIT
pragma solidity ^0.6.4;
pragma experimental ABIEncoderV2;

contract AddProduct {

    struct Product {
        address kopiAddress;
        string dataHash;
        string created_at;
    }

    Product[] public productArr;

    mapping (string => Product) private productsMapping;
    mapping (address => Product[]) private productsMappingByAddress;
    mapping (string => Product[]) private productsMappingByDate;
    mapping(address => mapping(string => bool)) private wallet;

    function tambahProduct(string memory dataHash, string memory created_at) public {
        Product memory prod = Product(msg.sender, dataHash, created_at);
        productArr.push(prod);
        productsMapping[prod.dataHash] = prod;
        productsMappingByAddress[msg.sender].push(prod);
        productsMappingByDate[created_at].push(prod);
        wallet[msg.sender][dataHash] = true;
    }

    function listProduct() public view returns (Product[] memory) {
        return productArr;
    }

    function detailproductByHash(string memory datahash) public view returns (Product memory)  {
        return (productsMapping[datahash]);
    }

    function lisProductByAddress() public view returns (Product[] memory) {
        return productsMappingByAddress[msg.sender];
    }

    function lisProductByDate(string memory created_at) public view returns (Product[] memory) {
        return productsMappingByDate[created_at];
    }
    
    function verify(string memory dataHash) public view returns (string memory) {
        if(wallet[msg.sender][dataHash]) {
            return "Verified";
        }
        return "Not Verified";
    }
}